public class Voiture_GPS extends  VoitureDecore {
    int prixGps =200;

    @Override
    public String description() {
        String nomvoiture = super.voiture.getClass().getSimpleName();
        return super.voiture.description()+" , GPS";
    }

    public Voiture_GPS(Voiture voiture) {
        super(voiture);
    }

    public int getPrixGps() {
        return prixGps;
    }

    public void setPrixGps(int prixGps) {
        this.prixGps = prixGps;
    }

    @Override
    public int calculerPrix() {
      return   super.calculerPrix()+prixGps;
    }

}
