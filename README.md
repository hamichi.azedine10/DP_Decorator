source : http://www.w3sdesign.com

The intent of the Decorator design pattern is to:
"Attach additional responsibilities to an object dynamically. Decorators provide a flexible
alternative to subclassing for extending functionality." [GoF]

• The Decorator design pattern solves problems like:
– How can responsibilities be added to an object dynamically?
– How can the functionality of an object be extended at run-time?

The terms responsibility, behavior, and functionality are usually interchangeable.
• "Sometimes we want to add responsibilities to individual objects, not to an entire class. A
graphical user interface toolkit, for example, should let you add properties like borders or
behaviors like scrolling to any user interface component." [GoF, p175]
• For example, reusable GUI/Web objects (like buttons, menus, or tree widgets).
It should be possible to add embellishments (i.e., borders, scroll bars, etc.) to basic GUI/Web
objects dynamically at run-time.
"In the Decorator pattern, embellishment refers to anything that adds responsibilities to an
object." [GoF, p47]

The Decorator design pattern provides a solution:
Define separate Decorator objects that add responsibilities to an object.
 Work through Decorator objects to extend the functionality of an object at run-time.
 