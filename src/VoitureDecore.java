public abstract class VoitureDecore extends Voiture {
    Voiture voiture ;

    public VoitureDecore(Voiture voiture) {
        this.voiture = voiture;
    }

    @Override
    public int calculerPrix() {
       return voiture.calculerPrix();
    }

}
