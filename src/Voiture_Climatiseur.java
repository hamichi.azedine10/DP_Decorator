public class Voiture_Climatiseur extends  VoitureDecore {
    private int prixClimatiseur =300;

    public Voiture_Climatiseur(Voiture voiture) {
        super(voiture);
    }
    @Override
    public String description() {
        String nomvoiture = super.voiture.getClass().getSimpleName();
        return nomvoiture+" avec l'option Climatiseur ";
    }
    public int getPrixClimatiseur() {
        return prixClimatiseur;
    }

    public void setPrixClimatiseur(int prixClimatiseur) {
        this.prixClimatiseur = prixClimatiseur;
    }

    @Override
    public int calculerPrix() {
        return super.calculerPrix()+ prixClimatiseur;
    }
}
